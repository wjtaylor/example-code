This repository contains some example code that might be useful to marketers.

"Finding your Competitors using Google Maps API" uses R to do some basic webscraping
via the Google Maps API.

"Mapping your Customers" uses zip code information to plot the customer distribution onto a U.S. map.